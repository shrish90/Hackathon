package com.hackathon.service;

import java.util.List;
import java.util.Map;

//import com.hackathon.model.Transactions;

public interface TransactionService {

	public List<Map<String, Object>> transactions(String userID);
}
