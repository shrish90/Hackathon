package com.hackathon.service.serviceImpl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hackathon.DAO.TransactionDao;
import com.hackathon.service.TransactionService;

@Service
public class TransactionServiceImpl implements TransactionService {

	@Autowired
	TransactionDao transactionDao;
	
	@Override
	public List<Map<String, Object>> transactions(String userID) {
		List<Map<String, Object>> transaction =transactionDao.transactions(userID);
		return transaction;
	}

}
