package com.hackathon.service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hackathon.DAO.TotalAmountDAO;
import com.hackathon.service.TotalAmountService;
@Service
public class TotalAmountServiceImpl implements TotalAmountService{

	@Autowired
	TotalAmountDAO totalAmountDAO;
	
	@Override
	public String totalAmount(String userID) {

		String totalAmount=totalAmountDAO.totalAmout(userID);
		return totalAmount;
	}


}
