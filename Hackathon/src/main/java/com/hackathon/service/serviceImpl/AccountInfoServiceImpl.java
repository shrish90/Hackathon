package com.hackathon.service.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hackathon.DAO.AccountInfoDAO;
import com.hackathon.Exceptions.InsufficientFundException;
import com.hackathon.Exceptions.UserNotFoundException;
import com.hackathon.model.Payment;
import com.hackathon.model.UserAccount;
import com.hackathon.service.AccountInfoService;
@Service
public class AccountInfoServiceImpl implements AccountInfoService {
	@Autowired
	AccountInfoDAO accountInfoDao;
	@Override
	public UserAccount getAccountInfo(String userID) throws UserNotFoundException{
		return accountInfoDao.getAccountInfo(userID);	
	}
	@Override
	public Payment completePayment(Payment payment) throws UserNotFoundException, InsufficientFundException{
		UserAccount senderAccount = accountInfoDao.getAccountInfo(payment.getSenderUserId());
		UserAccount recieverAccount = accountInfoDao.getAccountInfo(payment.getReceiverUserId());
		Payment paymentSuccess;
		if (senderAccount == null) {
			throw new UserNotFoundException(payment.getSenderUserId());
		} else if (recieverAccount == null) {
			throw new UserNotFoundException(payment.getReceiverUserId());
		} else if (senderAccount.getAvaliableAmount() < payment.getAmount()) {
			throw new InsufficientFundException();
		} else {
			paymentSuccess = accountInfoDao.completePayment(payment,senderAccount,recieverAccount);
		}
		return paymentSuccess;
	}
	@Override
	public List<UserAccount> getAllAcount() {
		
		return accountInfoDao.getAllAcount();
	}

}
