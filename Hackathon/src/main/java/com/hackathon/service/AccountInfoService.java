package com.hackathon.service;

import java.util.List;

import com.hackathon.Exceptions.InsufficientFundException;
import com.hackathon.Exceptions.UserNotFoundException;
import com.hackathon.model.Payment;
import com.hackathon.model.UserAccount;

public interface AccountInfoService {
	public UserAccount getAccountInfo(String userID) throws UserNotFoundException;
	public Payment completePayment(Payment payment) throws UserNotFoundException, InsufficientFundException;
	public List<UserAccount> getAllAcount();

}
