package com.hackathon.service;

public interface TotalAmountService {
	public String totalAmount(String userID);
}
