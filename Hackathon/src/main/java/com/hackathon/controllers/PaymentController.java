package com.hackathon.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

//import com.hackathon.Exceptions.GlobalExceptionHandler;
import com.hackathon.Exceptions.InsufficientFundException;
import com.hackathon.Exceptions.UserNotFoundException;
import com.hackathon.model.Payment;
import com.hackathon.model.UserAccount;
import com.hackathon.service.AccountInfoService;

@RestController
@RequestMapping("/paymentApi")
@CrossOrigin("*")
public class PaymentController {
	@Autowired
	AccountInfoService accountService;

	private static final Logger logger = LoggerFactory.getLogger(PaymentController.class);

	@RequestMapping(value = "/transfer", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Payment> payment(@Valid @RequestBody Payment payment)
			throws UserNotFoundException, InsufficientFundException {
			accountService.completePayment(payment);
			logger.info("Payment processed");
		//}
		return new ResponseEntity<>(payment, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/usersList", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String, Object>> users() {
		Map<String, Object> responseJSON = new HashMap<String, Object>();
		List<UserAccount> userList = accountService.getAllAcount();
		List<Map<String, Object>> userMapList = new ArrayList<Map<String, Object>>();
		for (UserAccount account : userList) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("name", account.getUserId());
			userMapList.add(map);
		}
		responseJSON.put("users", userMapList);
		return new ResponseEntity<>(responseJSON, HttpStatus.OK);
	}

}
