package com.hackathon.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hackathon.service.TotalAmountService;

@RestController
@RequestMapping("/totalAmountApi")
@CrossOrigin("*")
public class TotalAmountController {
	
	
	@Autowired
	TotalAmountService totalAmountService;
	
	@RequestMapping( value= "/totalAmout/{userID}", method= RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public String fetchtotalAmout(@PathVariable String userID){
		
		String totalAmount= totalAmountService.totalAmount(userID);
		
		return totalAmount;
	}

}
