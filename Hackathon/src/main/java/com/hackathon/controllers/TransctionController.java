package com.hackathon.controllers;

//import java.awt.PageAttributes.MediaType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hackathon.service.TransactionService;

@RestController
@RequestMapping("/transactionApi")
@CrossOrigin("*")
public class TransctionController {
	
	@Autowired
	TransactionService transactionService;

	@RequestMapping(value = "/transaction/{userID}" , method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String,Object>> transactions(@PathVariable("userID") String userID){
		List<Map<String, Object>> transaction = transactionService.transactions(userID);
		List<Map<String,String>> transactions = new ArrayList<Map<String,String>>();
		for(Map<String, Object> map: transaction) {
		Map<String,String> mock = new HashMap<String,String>();
		mock.put("amount", map.get("amount").toString());
		mock.put("description",map.get("description").toString());
		mock.put("type",map.get("tx_type").toString());
		mock.put("userID",map.get("user_id").toString());
		mock.put("timestamp",map.get("time_placed").toString());
		transactions.add(mock);
		}
		Map<String,Object> finalM = new HashMap<String,Object>();
		if(!transaction.isEmpty()) {
			finalM.put("availBalance",transaction.get(0).get("total_amount"));
		}else {
			finalM.put("availBalance",0.0);
		}
		
		finalM.put("response",transactions);
		return new ResponseEntity<> (finalM,HttpStatus.OK);
	}
}
