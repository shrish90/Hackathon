package com.hackathon.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/mock")
@CrossOrigin("*")
public class MockConroller {

	@RequestMapping("/transactions/{userID}")
	public ResponseEntity<Map<String,Object>> transactions(@PathVariable("userID") String userID){
		
		List<Map<String,String>> transactions = new ArrayList<Map<String,String>>();
		Map<String,String> mock = new HashMap<String,String>();
		mock.put("userID", "111");
		mock.put("type","D");
		mock.put("amount","1292.25");
		mock.put("description"," Transaction Description");
		mock.put("timestamp","19-11-2017 10:27:44");
		transactions.add(0, mock);
		Map<String,Object> finalM = new HashMap<String,Object>();
		finalM.put("response",transactions);
		return new ResponseEntity<Map<String,Object>> (finalM,HttpStatus.OK);
		
	
	}
}
