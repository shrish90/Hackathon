package com.hackathon.model;

public class UserAccount {

	Integer accountNumber;
	String userId;
	Double avaliableAmount;

	public Integer getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(Integer accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Double getAvaliableAmount() {
		return avaliableAmount;
	}

	public void setAvaliableAmount(Double avaliableAmount) {
		this.avaliableAmount = avaliableAmount;
	}
}
