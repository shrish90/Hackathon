package com.hackathon.DAO;


import java.util.List;

import com.hackathon.Exceptions.UserNotFoundException;
import com.hackathon.model.Payment;
import com.hackathon.model.UserAccount;

public interface AccountInfoDAO {
	public UserAccount getAccountInfo(String userID) throws UserNotFoundException;
	public Payment completePayment(Payment payment,UserAccount sender,UserAccount reciever);
	public List<UserAccount> getAllAcount();
	
}
