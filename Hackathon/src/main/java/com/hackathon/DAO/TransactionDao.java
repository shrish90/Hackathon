package com.hackathon.DAO;

import java.util.List;
import java.util.Map;

public interface TransactionDao {
	public List<Map<String, Object>> transactions(String userID);
}
