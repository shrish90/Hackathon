package com.hackathon.DAO.DAOImpl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.hackathon.DAO.TotalAmountDAO;
@Repository
public class TotalAmountDAOImpl implements TotalAmountDAO{

	private final static String totalAmout="Select total_amount from ops.user_account where user_id=?";
	
	@Autowired
	JdbcTemplate jdbcTemplate; 
	@Override
	public String totalAmout(String userID) {

		List<Map<String, Object>> allrecords= jdbcTemplate.queryForList(totalAmout,userID);	
		return allrecords.get(0).get("total_amount").toString();
	}

}
