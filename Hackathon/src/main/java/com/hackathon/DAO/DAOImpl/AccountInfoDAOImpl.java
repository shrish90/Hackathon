package com.hackathon.DAO.DAOImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.hackathon.DAO.AccountInfoDAO;
import com.hackathon.Exceptions.UserNotFoundException;
import com.hackathon.model.Payment;
import com.hackathon.model.UserAccount;

@Repository
public class AccountInfoDAOImpl implements AccountInfoDAO {
	@Autowired
	JdbcTemplate jdbcTemplate;

	private static final String sql = "SELECT account_number,user_id,total_amount FROM USER_ACCOUNT WHERE user_id =?";
	private static final String updateAccountSQL = "UPDATE USER_ACCOUNT SET total_amount =? WHERE account_number =?";
	private static final String getAllSql = "SELECT account_number,user_id,total_amount FROM USER_ACCOUNT";

	@Override
	public UserAccount getAccountInfo(String userID) throws UserNotFoundException {
		UserAccount userAccount = new UserAccount();
		List<Map<String, Object>> account = jdbcTemplate.queryForList(sql, userID);
		if (account.isEmpty()) {
			return null;
		} else {
			Map<String, Object> accountMap = account.get(0);
			userAccount.setAccountNumber((Integer) accountMap.get("account_number"));
			userAccount.setAvaliableAmount((Double) accountMap.get("total_amount"));
			userAccount.setUserId(userID);
		}
		return userAccount;

	}

	@Override
	public synchronized Payment completePayment(Payment payment, UserAccount sender, UserAccount reciever) {
		SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(jdbcTemplate).withTableName("transaction_history");
		// Debit
		Map<String, Object> DebitMap = new HashMap<String, Object>();
		DebitMap.put("account_number", sender.getAccountNumber());
		DebitMap.put("amount", payment.getAmount());
		DebitMap.put("description", payment.getDescription());
		DebitMap.put("time_placed", new Date());
		DebitMap.put("tx_type", "D");
		jdbcInsert.execute(DebitMap);
		// Credit
		Map<String, Object> creditMap = new HashMap<String, Object>();
		creditMap.put("account_number", reciever.getAccountNumber());
		creditMap.put("amount", payment.getAmount());
		creditMap.put("description", payment.getDescription());
		creditMap.put("time_placed", new Date());
		creditMap.put("tx_type", "C");
		jdbcInsert.execute(creditMap);
		// Substract Amount
		jdbcTemplate.update(updateAccountSQL, (sender.getAvaliableAmount() - payment.getAmount()),
				sender.getAccountNumber());
		// add amount
		jdbcTemplate.update(updateAccountSQL, (reciever.getAvaliableAmount() + payment.getAmount()),
				reciever.getAccountNumber());
		return null;
	}

	@Override
	public List<UserAccount> getAllAcount() {
		
		List<Map<String, Object>> account = jdbcTemplate.queryForList(getAllSql);
		List<UserAccount> accountList = new ArrayList<UserAccount>();
		if (account.isEmpty()) {
			return null;
		} else {
			for(Map<String, Object> accountMap:account) {
				UserAccount userAccount = new UserAccount();
				userAccount.setAccountNumber((Integer) accountMap.get("account_number"));
				userAccount.setAvaliableAmount((Double) accountMap.get("total_amount"));
				userAccount.setUserId(accountMap.get("user_id")+"");
				accountList.add(userAccount);
			}
			
			
		}
		return accountList;
	}

}
