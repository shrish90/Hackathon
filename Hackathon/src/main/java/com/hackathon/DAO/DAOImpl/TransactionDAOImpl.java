package com.hackathon.DAO.DAOImpl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.hackathon.DAO.TransactionDao;

@Repository
public class TransactionDAOImpl implements TransactionDao {

	@Autowired
	JdbcTemplate jdbcTemplate;

	private static final String selectsql = "SELECT  th.account_number, th.amount, th.description, th.time_placed, th.tx_type, ua.user_id, ua.total_amount FROM ops.transaction_history th, ops.user_account ua where th.account_number=ua.account_number and ua.user_id=?";

	@Override
	public List<Map<String, Object>> transactions(String userID) {
		return jdbcTemplate.queryForList(selectsql, userID);
	}

}
