package com.hackathon.Exceptions;

public class UserNotFoundException extends Exception{
	
	static final long serialVersionUID = 1768160417381863330L;

	public UserNotFoundException(String userId) {
		super("User with user id: "+userId+" is not Avaliable in Database");
	}
	}
