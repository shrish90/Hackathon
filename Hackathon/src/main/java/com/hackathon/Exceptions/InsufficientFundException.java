package com.hackathon.Exceptions;

public class InsufficientFundException extends Exception{

	private static final long serialVersionUID = 6951505667383136924L;

	public InsufficientFundException()
		{
			super("User account is not having sufficient amount for this transaction");
		}
	}
