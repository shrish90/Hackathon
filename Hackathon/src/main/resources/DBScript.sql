create database OPS;



CREATE TABLE `OPS`.`USER_ACCOUNT` 
(
`account_number` INT NOT NULL AUTO_INCREMENT,
  
`user_id` VARCHAR(45) NOT NULL,
  
`total_amount` DOUBLE NOT NULL,

  PRIMARY KEY (`account_number`));

CREATE TABLE `OPS`.`TRANSACTION_HISTORY` 
(user_account
`tx_num` INT NOT NULL AUTO_INCREMENT,
`account_number` INT NOT NULL,
`amount` DOUBLE NOT NULL,
`description` VARCHAR(250) NOT NULL,
`time_placed` datetime Not NULL,
  PRIMARY KEY (`tx_num`),
  FOREIGN KEY(account_number) REFERENCES USER_ACCOUNT(account_number)
  );
 
 ALTER TABLE `OPS`.`TRANSACTION_HISTORY` DROP `total_amount`;
 ALTER TABLE `OPS`.`TRANSACTION_HISTORY` ADD `tx_type` varchar(50);
 
 
commit;